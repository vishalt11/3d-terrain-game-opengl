#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <cstdlib>
#include <cmath>
#include <math.h> 
#include <sstream>  
#include <string> 
#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include "imageloader.h"
#include "vec3f.h"
#define PI 3.141592653589
#define DEG2RAD(deg) (deg * PI / 180)
using namespace std;

float power=1.0;
float X[3]={0,0,0};
float Y[3]={0,0,0};
float Z[3]={0,0,0};
int win_width = 800, win_height = 400;
float cx = 0.0, cy = 0.0; // initially 5 units south of origin
float deltaMove = 0.0; // initially camera doesn't move
float lx = 0.0, ly = 1.0; // camera points initially along y-axis
float anglecam = 0.0; // angle of rotation for the camera direction
float deltaAngley = 0.0; 
float deltaAnglex = 0.0;
float theta = -75.5;
float true_power;
float true_angle;
// additional angle change when dragging
// Mouse drag control
int isDraggingy = 0;
int isDraggingx = 0; // true when dragging
int xDragStart = 0; // records the x-coordinate when dragging starts
int yDragStart = 0; // records the x-coordinate when dragging starts
float topAngle=0;
float vel_x=0,vel_z=0,vel_y=0;
GLUquadricObj *obj = gluNewQuadric();

//Represents a terrain, by storing a set of heights and normals at 2D locations
class Terrain {
	private:
		int w; //Width
		int l; //Length
		float** hs; //Heights
		Vec3f** normals;
		bool computedNormals; //Whether normals is up-to-date
	public:
		Terrain(int w2, int l2) {
			w = w2;
			l = l2;
			
			hs = new float*[l];
			for(int i = 0; i < l; i++) {
				hs[i] = new float[w];
			}
			
			normals = new Vec3f*[l];
			for(int i = 0; i < l; i++) {
				normals[i] = new Vec3f[w];
			}
			
			computedNormals = false;
		}
		
		~Terrain() {
			for(int i = 0; i < l; i++) {
				delete[] hs[i];
			}
			delete[] hs;
			
			for(int i = 0; i < l; i++) {
				delete[] normals[i];
			}
			delete[] normals;
		}
		
		int width() {
			return w;
		}
		
		int length() {
			return l;
		}
		
		//Sets the height at (x, z) to y
		void setHeight(int x, int z, float y) {
			hs[z][x] = y;
			computedNormals = false;
		}
	
	//Returns the height at (x, z)
	float getHeight(int x, int z) {
		return hs[z][x];
	}
	
	//Computes the normals, if they haven't been computed yet
		void computeNormals() {
			if (computedNormals) {
				return;
			}
			
			//Compute the rough version of the normals
			Vec3f** normals2 = new Vec3f*[l];
			for(int i = 0; i < l; i++) {
				normals2[i] = new Vec3f[w];
			}
			
			for(int z = 0; z < l; z++) {
				for(int x = 0; x < w; x++) {
					Vec3f sum(0.0f, 0.0f, 0.0f);
					
					Vec3f out;
					if (z > 0) {
						out = Vec3f(0.0f, hs[z - 1][x] - hs[z][x], -1.0f);
					}
					Vec3f in;
					if (z < l - 1) {
						in = Vec3f(0.0f, hs[z + 1][x] - hs[z][x], 1.0f);
					}
					Vec3f left;
					if (x > 0) {
						left = Vec3f(-1.0f, hs[z][x - 1] - hs[z][x], 0.0f);
					}
					Vec3f right;
					if (x < w - 1) {
						right = Vec3f(1.0f, hs[z][x + 1] - hs[z][x], 0.0f);
					}
					
					if (x > 0 && z > 0) {
						sum += out.cross(left).normalize();
					}
					if (x > 0 && z < l - 1) {
						sum += left.cross(in).normalize();
					}
					if (x < w - 1 && z < l - 1) {
						sum += in.cross(right).normalize();
					}
					if (x < w - 1 && z > 0) {
						sum += right.cross(out).normalize();
					}
					
					normals2[z][x] = sum;
				}
			}
			
			//Smooth out the normals
			const float FALLOUT_RATIO = 0.5f;
			for(int z = 0; z < l; z++) {
				for(int x = 0; x < w; x++) {
					Vec3f sum = normals2[z][x];
					
					if (x > 0) {
						sum += normals2[z][x - 1] * FALLOUT_RATIO;
					}
					if (x < w - 1) {
						sum += normals2[z][x + 1] * FALLOUT_RATIO;
					}
					if (z > 0) {
						sum += normals2[z - 1][x] * FALLOUT_RATIO;
					}
					if (z < l - 1) {
						sum += normals2[z + 1][x] * FALLOUT_RATIO;
					}
					
					if (sum.magnitude() == 0) {
						sum = Vec3f(0.0f, 1.0f, 0.0f);
					}
					normals[z][x] = sum;
				}
			}
			
			for(int i = 0; i < l; i++) {
				delete[] normals2[i];
			}
			delete[] normals2;
			
			computedNormals = true;
		}
		
		//Returns the normal at (x, z)
		Vec3f getNormal(int x, int z) {
			if (!computedNormals) {
				computeNormals();
			}
			return normals[z][x];
		}
};

//Loads a terrain from a heightmap.  The heights of the terrain range from
//-height / 2 to height / 2.
Terrain* loadTerrain(const char* filename, float height) {
	Image* image = loadBMP(filename);
	Terrain* t = new Terrain(image->width, image->height);
	for(int y = 0; y < image->height; y++) {
		for(int x = 0; x < image->width; x++) {
			unsigned char color =
				(unsigned char)image->pixels[3 * (y * image->width + x)];
			float h = height * ((color / 255.0f) - 0.5f);
			t->setHeight(x, y, h);
		}
	}
	
	delete image;
	t->computeNormals();
	return t;
}

float _angle = 60.0f;
Terrain* terrain;

void cleanup() {
	delete terrain;
}
int flag=0;
int flag_shot=0;
float topdx = 6.0f, topdy = -16.0f;
void handleKeypress(unsigned char key, int x, int y) 
{
	if (key == 27 || key == 'q' || key == 'Q') exit(0);

	if((key == 'w') && (power <= 10.0) && (flag_shot==0))
	{
		power +=0.5;
	}
	if((key == 's') && (power >= 1.0) && (flag_shot==0))
	{
		power -=0.5;
	}
	if (key == 32)
	{
		flag_shot = 1;
	}
	if(key=='p'){
		flag = 0;
		X[0]=-10; X[1]=5; X[2]=0;
		Y[0]=0; Y[1]=0; Y[2]=0;
		Z[0]=1; Z[1]=0; Z[2]=0;
	}
	if(key=='t'){
		flag = 0;
		X[0]=0; X[1]=14; X[2]=0;
		Y[0]=0; Y[1]=0; Y[2]=0;
		Z[0]=1; Z[1]=0; Z[2]=0;
	}
	if(key=='h'){
		flag = 3;
	}
	if(key=='f'){
		flag = 4;
	}
	if(key == 'r')
	{
		topdx =6.0;
		topdy = -16.0;
		vel_x = 0.0;
		vel_y = 0.0;
		power = 0.0;
		theta = -75.5;
		flag_shot = 0;
	}
}

void reset()
{
	topdx =6.0;
	topdy = -16.0;
	vel_x = 0.0;
	vel_y = 0.0;
	power = 0.0;
	theta = -75.5;
	flag_shot = 0;
}
void initRendering() {
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);
	glShadeModel(GL_SMOOTH);
}

void handleResize(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, (double)w / (double)h, 1.0, 200.0);
}




int slices =50;
int stacks =50; 


void drawtop()
{
	//	float topdyp = -(topdy);
	//	float height = terrain->getHeight(topdx,topdyp);
		//printf("height = %f\n", height);
		//glRotatef(-90,1,0,0);
		GLfloat cyan[] = {0.f, .8f, .8f, 1.f};
		glMaterialfv(GL_FRONT, GL_DIFFUSE, cyan);
	//	glTranslatef(topdx,topdy,height+1.5);
		
			
		//glRotatef(topAngle/5,0,0,1);
		
		glPushMatrix();
		//glRotatef( 15 , 0 , 1 , 0 );
		glRotatef(topAngle,0,0,1);

		glColor3f(1.0, 0.5, 0.0);
		glutSolidTorus( 0.25,0.55, 100,100);
		glColor3f(0.0,0.0,0.0);
		glutWireTorus( 0.25,0.55, 15, 60);

		glPushMatrix();
			glTranslatef(0,0,0.0);
			glRotatef(180,0,1,0);
			glutSolidCone(0.2, 1.5, 10, 2);
		glPopMatrix();

		glPushMatrix();
			glTranslatef(0,0,0.2);
			glColor3f(1.0, 0.5, 0.0);
			glutSolidTorus( 0.1,0.2, 25, 30);
			glColor3f(0,0,0);
			glutWireTorus( 0.1,0.2, 15, 60);
			glPopMatrix();
		glColor3f(0.7, 0.7, 0.5);

		glPushMatrix();
			glTranslatef(0,0,-0.5);
			glutSolidTorus( 0.2,0.2, 25, 30);
			glColor3f(0.5,.5,1);
			glutWireTorus( 0.2,0.2, 15, 60);
		glPopMatrix();
		
		glTranslatef(0,0,0.5);

		glPushMatrix();
			glTranslatef(0,0,0.3);
		glPopMatrix();
		glPushMatrix();	
			glTranslatef(0,0,-0.5);
			glColor3f(0.0, 1.0, 1.0);
			gluCylinder(obj ,0.1,0.1,0.6,slices,stacks);
		glPopMatrix();

		glPopMatrix();
}

float targx = 58.0;
float targy = -10.0;
void drawtarget()
{
	
	glRotatef(-60,1,0,0);
	GLfloat cyan[] = {0.f, .8f, .8f, 1.f};
	glMaterialfv(GL_FRONT, GL_DIFFUSE, cyan);
	//glScalef(3,3,3);
	glPushMatrix();
		//float height = terrain->getHeight(58.0,10.0);
		glPushMatrix();
			glColor3f(1,1,1);
			glTranslatef(targx,targy,10);
			glRotatef(-90,0,1,0);
			gluCylinder( obj, 4.0,0.1,0.1,slices,stacks);
		glPopMatrix();
		glPushMatrix();
			glColor3f(0,0,0);
			glTranslatef(targx - 0.05,targy,10);
			glRotatef(-90,0,1,0);
			gluCylinder( obj, 3.4,0.1,0.1,slices,stacks);
		glPopMatrix();
		glPushMatrix();
			glColor3f(1,1,1);
			glTranslatef(targx - 0.1,targy,10);
			glRotatef(-90,0,1,0);
			gluCylinder( obj, 2.8,0.1,0.1,slices,stacks);
		glPopMatrix();
		glPushMatrix();
			glColor3f(0,0,0);
			glTranslatef(targx - 0.15,targy,10);
			glRotatef(-90,0,1,0);
			gluCylinder( obj, 2.2,0.1,0.1,slices,stacks);
		glPopMatrix();
		glPushMatrix();
			glColor3f(1,1,1);
			glTranslatef(targx - 0.2,targy,10);
			glRotatef(-90,0,1,0);
			gluCylinder( obj, 1.6,0.0,0.1,slices,stacks);
		glPopMatrix();
	
	glPopMatrix();
}


int score = 10;
void update(int value) {
	topAngle += 1.0f;
	true_angle = theta + 75.5;
	true_power = power/8;
	vel_y = true_power*sin(DEG2RAD(true_angle));
	vel_x = true_power*cos(DEG2RAD(true_angle));
	if((flag_shot == 1) && (topdx <= 58.0) && (topdy >= -58.0) && (topdy <= -0.5) && (topdx >= 0.5) )
	{
		topdx += vel_x;
		topdy += (-vel_y);
	}
	if(((topdx + 0.55) >= 58.0) && ( topdy <= -10 ) && (topdy >= -17)) 
	{
		score += 10;
		reset();
	}
	if((topdx >= 58.0))
	{	
		score -=1;
		reset();
	}
	if(topdx <= 0.5)
	{
		score -=1;
		reset();
	}
	if(topdy >= -0.5)
	{
		score -=1;
		reset();
	}
	if(topdy <= -58.0)
	{
		score -=1;
		reset();
	}
	glutPostRedisplay();
	glutTimerFunc(25, update, 0);
}

void renderBitmapString(float x,float y,float z,void *font,char *string) 
{
	char *c;
	glRasterPos3f(x,y,z);
	for (c=string; *c != '\0'; c++) 
	{
		glutBitmapCharacter(font, *c);
	}
}

void drawScene() 
{
	glClearColor(0.0, 0.7, 1.0, 1.0); // sky color is light blue
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(X[0],X[1],X[2],Y[0],Y[1],Y[2],Z[0],Z[1],Z[2]);

	//printf("%d\n",score);
	
	std::ostringstream score1; 
	score1 << score; 
	std::string buffer1 = score1.str(); 

	char fscore[]="SCORE:";
	renderBitmapString(0,0,-6.6,GLUT_BITMAP_HELVETICA_18,fscore);

	glRasterPos3f(0,0,-5.5);
	for(int i = 0; buffer1[i] != '\0'; i++)
		glutBitmapCharacter( GLUT_BITMAP_HELVETICA_18 , buffer1[i]);

	glScalef(2,2,2);
	GLfloat ambientColor[] = {0.4f, 0.4f, 0.4f, 1.0f};
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);
	
	GLfloat lightColor0[] = {0.6f, 0.6f, 0.6f, 1.0f};
	GLfloat lightPos0[] = {-0.5f, 0.8f, 0.1f, 0.0f};
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor0);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);
	
	float scale = 5.0f / max(terrain->width() - 1, terrain->length() - 1);
	glScalef(scale, scale, scale);
	glTranslatef(-(float)(terrain->width() - 1) / 2,
				 0.0f,
				 -(float)(terrain->length() - 1) / 2);

	float topdyp = -(topdy);
	float height = terrain->getHeight(topdx,topdyp);
	if(flag == 4)
	{
		//glTranslatef((float)(terrain->width() - 1) / 2,0,(float)(terrain->length() - 1) / 2);
		gluLookAt(
				topdx, topdy, height,
				0,0,0,
				1,0,0
			 );

	}

	glColor3f(0.0f, 0.7f, 0.0f);
	for(int z = 0; z < terrain->length() - 1; z++) 
	{
		//Makes OpenGL draw a triangle at every three consecutive vertices
		glBegin(GL_TRIANGLE_STRIP);
		for(int x = 0; x < terrain->width(); x++) 
		{
			Vec3f normal = terrain->getNormal(x, z);
			glNormal3f(normal[0], normal[1], normal[2]);
			glVertex3f(x, terrain->getHeight(x, z), z);
			normal = terrain->getNormal(x, z + 1);
			glNormal3f(normal[0], normal[1], normal[2]);
			glVertex3f(x, terrain->getHeight(x, z + 1), z + 1);
		}
		glEnd();
	}
	
	glPushMatrix();
		//glScalef(1,1,1);
		
		//Vec3f normal=terrain->getNormal(topdx,topdyp).normalize();
		glRotatef(-90,1,0,0);

		glTranslatef(topdx,topdy,height+1.5);
		
		drawtop();
	glPopMatrix();
	glPushMatrix();
		drawtarget();
	glPopMatrix();
	glPushMatrix();
		glTranslatef(0,15,10);
		glRotatef(theta,1,0,0);
		glColor3f(0.0, 1.0, 1.0);
		gluCylinder(obj ,0.1,0.1,power,slices,stacks);
	glPopMatrix();
	glutSwapBuffers();
}



void pressSpecialKey(int key, int xx, int yy)
{
	
	if(key == GLUT_KEY_UP)
	{
		deltaMove = 1.0;
	}
	if(key == GLUT_KEY_DOWN)
	{
		deltaMove = -1.0;
	}
	if((key == GLUT_KEY_LEFT) && (theta >= -100.5) &&(flag_shot==0))
	{
		theta -= 1.0f;
	}
	if((key == GLUT_KEY_RIGHT) && (theta <= -50.5f) &&(flag_shot==0))
	{
		theta += 1.0f;
	}
} 

void releaseSpecialKey(int key, int x, int y) 
{
	switch (key) {
		case GLUT_KEY_UP : deltaMove = 0.0; break;
		case GLUT_KEY_DOWN : deltaMove = 0.0; break;
	}
} 

int f=0;

void mouseMove(int x, int y) 
{ 	
	if (flag == 3){
		if (isDraggingy) 
		{ 
		deltaAngley = (y - yDragStart) * 0.005;
		X[0]=-10; X[1]=5; X[2]=0;
		Y[0]=deltaAngley; Y[1]=deltaAngley; Y[2]=0;
		Z[0]=1; Z[1]=0; Z[2]=0;
		}
		if (isDraggingx)
		{
		deltaAnglex = (x - xDragStart) * 0.005;
		X[0]=-10; X[1]=5; X[2]=0;
		Y[0]=0; Y[1]=deltaAnglex; Y[2]=deltaAnglex;
		Z[0]=0; Z[1]=1; Z[2]=0;
		}
	}
}


void mouseButton(int button, int state, int x, int y) 
{
	if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN) { // left mouse button pressed
			isDraggingy = 1; // start dragging
			yDragStart = y; // save x where button first pressed
			//xDragStart = x;
			isDraggingx = 0;
			//f=0;
		}
	}
	if (button == GLUT_RIGHT_BUTTON) {
		if (state == GLUT_DOWN) { // left mouse button pressed
			isDraggingx = 1; // start dragging
			xDragStart = x; // save x where button first pressed
			//xDragStart = x;
			isDraggingy = 0;
			//f=1;
		}
	}
}

int main(int argc, char** argv) {
	X[0]=-10; X[1]=5; X[2]=0;
	Y[0]=0; Y[1]=0; Y[2]=0;
	Z[0]=1; Z[1]=0; Z[2]=0;
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(1366, 768);
	
	glutCreateWindow("TOP-3d-Assignment-4");
	initRendering();
	
	terrain = loadTerrain("heightmap3.bmp", 10);
	
	glutDisplayFunc(drawScene);
	glutKeyboardFunc(handleKeypress);
	glutReshapeFunc(handleResize);
	glutSpecialFunc(pressSpecialKey); // process special key pressed
	glutMouseFunc(mouseButton); // process mouse button push/release
	glutMotionFunc(mouseMove); // process mouse dragging motion
	glutSpecialUpFunc(releaseSpecialKey); // process special key release
	glutTimerFunc(25, update, 0);
	
	glutMainLoop();
	return 0;
}









